﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ProductAPIService.Models;

namespace ProductAPIService.Controllers
{
    /// <summary>
    /// Product controller class
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/Product")]
    public class ProductController : ApiController
    {
        private ProductDBEntities db = new ProductDBEntities();

        /// <summary>
        /// Get all products
        /// </summary>
        /// <returns>List of products</returns>
        [Route("GetAll")]
        [HttpGet]
        public IQueryable<Product> GetAllProducts()
        {
            return db.Products;
        }
        /// <summary>
        /// Get product by ID
        /// </summary>
        /// <param name="productId">ProductID</param>
        /// <returns>Product</returns>
        [Route("Get/{productId}", Name ="GetProduct")]
        [HttpGet]
        public IHttpActionResult GetProduct(int productId)
        {
            Product product = db.Products.Find(productId);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }

        /// <summary>
        /// Create product
        /// </summary>
        /// <param name="product">Product Model</param>
        /// <returns>Product</returns>
        [Route("Create")]
        [HttpPost]
        public IHttpActionResult CreateProduct(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Products.Add(product);
            db.SaveChanges();

            return Ok(product);
        }

        /// <summary>
        /// Update Product
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <param name="product">Product Model</param>
        /// <returns>Product</returns>
        [Route("update")]
        [HttpPut]
        public IHttpActionResult UpdateProduct(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productModel = db.Products.Where(s => s.ProductID == product.ProductID).FirstOrDefault();

            if(productModel != null)
            {
                productModel.ProductName = product.ProductName;
                productModel.Description = product.Description;
                productModel.Price = product.Price;

                db.SaveChanges();
            }
            else
            {
                return NotFound();
            }

            return Ok(productModel);
        }

        /// <summary>
        /// Delete Product
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <returns>product</returns>
        [Route("delete/{productId}")]
        public IHttpActionResult DeleteProduct(int productId)
        {
            Product product = db.Products.Find(productId);
            if (product == null)
            {
                return NotFound();
            }
            db.Products.Remove(product);
            db.SaveChanges();
            return Ok(product);
        }
    }
}
