﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ProductAPIService.Models;

namespace ProductAPIService.Controllers
{
    /// <summary>
    /// Category controller class
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/Category")]
    public class CategoryController : ApiController
    {
        private ProductDBEntities db = new ProductDBEntities();

        /// <summary>
        /// Get all categories
        /// </summary>
        /// <returns>List of categorry</returns>
        [Route("GetAll")]
        [HttpGet]
        public IQueryable<Category> GetAllCategories()
        {
            return db.Categories;
        }
    }
}
